#!/usr/bin/env python3

import os, sys

import argparse
import json
import subprocess

def customenv(var):
    return os.getenv('CUSTOM_ENV_{}'.format(var))

if __name__ == '__main__':
    try:
        job_id = customenv('CI_JOB_ID')
        container_name = 'gitlab-runner-{}'.format(job_id)

        if sys.argv[1] == 'config':
            project_id = customenv('CI_CONCURRENT_PROJECT_ID')
            project_path = customenv('CI_PROJECT_PATH_SLUG')
            json.dump({
                'builds_dir': '/builds/{}/{}'.format(project_id, project_path),
                'cache_dir': '/cache/{}/{}'.format(project_id, project_path),
                'builds_dir_is_shared': True,
                'hostname': 'podman-runner',
                'driver': {
                    'name': 'podman driver',
                    'version': '0.1'
                }
            }, sys.stdout)

        elif sys.argv[1] == 'prepare':
            arch = os.uname().machine
            if arch == 'x86_64':
                image = customenv('DOCKER_AMD64')
            elif arch == 'aarch64':
                image = customenv('DOCKER_ARM64')
            else:
                print('unknown architecture {}'.format(arch))
                exit(1)

            subprocess.check_call(['podman', 'pull', image])

            subprocess.check_call(['podman', 'run', '--privileged', '--detach', '--tty',
                                   '--name={}'.format(container_name),
                                   image, '/bin/bash'])

        elif sys.argv[1] == 'run':
            script, substage = sys.argv[2:]

            subprocess.check_call(['podman', 'cp', script,
                                   '{}:/tmp/gitlab-script.sh'.format(container_name)])

            subprocess.check_call(['podman', 'exec',
                                   container_name,
                                   '/bin/bash', '-c',
                                   '/tmp/gitlab-script.sh'])

        elif sys.argv[1] == 'cleanup':
            subprocess.check_call(['podman', 'stop', container_name])
            subprocess.check_call(['podman', 'rm', container_name])

        else:
            print('Unknown command {}'.format(sys.argv[1]))
            exit(1)
    except subprocess.CalledProcessError as e:
        exit(e.returncode)
